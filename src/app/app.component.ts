import { Component, HostListener } from '@angular/core';
import { MatFormFieldControl } from '@angular/material/form-field';
import { Router, NavigationStart } from '@angular/router';
import { AuthService } from './services/auth-service/auth.service';
import * as firebase from 'firebase/app';
import { FirestoreService } from './services/database-service/firestore/firestore.service';


@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
  providers: [{ provide: MatFormFieldControl, useExisting: AppComponent }],
})

export class AppComponent {

  title = 'tocotoco-admin';
  status = false;
  admin;
  user;

  route;
  constructor(
    private router: Router,
    private authService: AuthService,
    private ftService: FirestoreService,
  ) {
    this.loadDataUser();
  }

  logout() {
    this.authService.logout();
    this.status = !this.status;
  }

  loadDataUser() {
    this.router.events.subscribe(event => {
      if (event instanceof NavigationStart) {
        this.route = event.url;
        console.log(this.route);
      };
    });

    // this.authService.checkoutAdmin().onAuthStateChanged(user => {
    //   this.ftService.getDataCheckadmin(user).then(user1 => {
    //     let data = user1.data();
    //     if (data.role == 'user') {
    //       console.log('Là user');
    //     } else if (data.role == 'admin') {
    //       console.log('Là admin');
    //     }
    //   })
    // })
  }

  show_Admin_info() {
    this.status = !this.status;
  }

  // @HostListener('click') onclick(event) {
  //   console.log('component is clicked');
  //   console.log(event);
  // }
}

