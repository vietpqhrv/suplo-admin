import { Component, OnInit } from '@angular/core';
import { AuthService } from 'src/app/services/auth-service/auth.service';
import { Router } from '@angular/router';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import * as firebase from 'firebase';


@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {
  errorlogin = false;
  form: FormGroup;
  handle;
  loading = false;
  constructor(
    public authService: AuthService,
    private router: Router,
    private fbd: FormBuilder
  ) {
    // window.recaptchaVerifier = new firebase.auth.RecaptchaVerifier('recaptcha-container', {
    //   'size': 'normal',
    //   'callback': function(response) {
    //     // reCAPTCHA solved, allow signInWithPhoneNumber.
    //     // ...
    //   },
    //   'expired-callback': function() {
    //     // Response expired. Ask user to solve reCAPTCHA again.
    //     // ...
    //   }
    // });
  }
  ngOnInit() {

    this.form = this.fbd.group({
      email: ['', [Validators.required, Validators.email]],
      password: ['', [Validators.required, Validators.minLength(6)]]
    });
  }

  onSubmit() {
    this.loading = true;
    this.authService.login(this.form.value.email, this.form.value.password).then(data => {
      this.router.navigate(['/users/list']);
      this.loading = false;
    }).catch(err => {
      this.errorlogin = true;
      this.loading = false;
    });
  }

  login() {

  }

  logout() {
    this.authService.logout();
  }

}
