import { Component, OnInit, NgZone } from '@angular/core';
import { AuthService } from 'src/app/services/auth-service/auth.service';
import { Router } from '@angular/router';
import * as firebase from 'firebase/app';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MustMatch } from '../check-pass/check-pass.validator';
import { AngularFirestore, AngularFirestoreDocument } from '@angular/fire/firestore';
import { MatSnackBar } from '@angular/material';



@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.scss']
})
export class RegisterComponent implements OnInit {
  form: FormGroup;
  submitted = false;
  loading = false;
  forms;
  error;
  handle;
  uid;
  firstName;
  lastName;
  passWord;
  succeed = 'Đăng ký thành công';
  failed = 'Thất bại';
  durationInSeconds = 3;
  constructor(
    public authService: AuthService,
    private fbd: FormBuilder,
    private afs: AngularFirestore,
    private snackBar: MatSnackBar,
  ) {

  }

  ngOnInit() {
    this.form = this.fbd.group({
      firstName: ['', [Validators.required]],
      lastName: ['', [Validators.required]],
      email: ['', [Validators.required, Validators.email]],
      passWord: ['', [Validators.required, Validators.minLength(6)]],
      confirmPassword: ['', Validators.required]
    },
      {
        validator: MustMatch('passWord', 'confirmPassword')
      }
    );

  }

  get f() { return this.form.controls; }

  //đăng ký
  onSubmit() {
    this.forms = this.form.value;
    this.firstName = this.forms.firstName;
    this.lastName = this.forms.lastName;
    this.passWord = this.forms.passWord;
    console.log(this.forms);

    this.submitted = true;
    this.loading = true;
    this.error = this.form.controls;
    // // stop here if form is invalid
    if (this.form.invalid) {
      console.log(this.error);
      this.loading = false;
      return;
    }

    this.authService.register(this.forms.email, this.forms.passWord).then(data => {
      console.log(data.user.uid);
      this.updateUserData(data.user);
    }).catch(error => {
      console.log('đăng ký thất bại');
      this.loading = false;
    });
  }


  // lưu thông tin vào database
  private updateUserData(user) {
    console.log(user);
    const userRef = this.afs.doc(`users/${user.uid}`);
    const data = {
      uid: user.uid,
      email: user.email,
      firstname: this.firstName,
      lastnam: this.lastName,
      password: this.passWord,
      role: 'user',
    }

    userRef.set(data, { merge: true });
    console.log('thêm vào database thành công');
    this.loading = false;
    this.openSnackBar('Đăng ký thành công');
  }

  // hiện thông báo dăng ký thành công
  openSnackBar(ád) {
    this.snackBar.open(ád, '', {
      duration: 2000,
    });
  }
}
