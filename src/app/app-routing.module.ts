import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LoginComponent } from './account/login/login.component';
import { AuthComponent } from './account/auth/auth.component';
import { RegisterComponent } from './account/register/register.component';
import { AuthGuard } from './services/guard/user-guard/auth.guard';
import { AdminAuthGuard } from './services/guard/admin-guard/admin-auth.guard';
import { UserListComponent } from './pages/users/user-list/user-list.component';
import { UserDetailComponent } from './pages/users/user-detail/user-detail.component';
import { KiotListComponent } from './pages/kiots/kiot-list/kiot-list.component';

const routes: Routes = [
  { path: 'login', component: LoginComponent },
  { path: 'auth', component: AuthComponent },
  { path: 'register', component: RegisterComponent },
  { path: 'users/list', component: UserListComponent, canActivate: [AuthGuard] },
  { path: 'kiots/list', component: KiotListComponent , canActivate: [AuthGuard]},
  { path: 'user/:email', component: UserDetailComponent },
  // , canActivate: [AdminAuthGuard]
  { path: '**', redirectTo: '', pathMatch: 'full', canActivate: [AuthGuard] },
  { path: '', redirectTo: '', pathMatch: 'full', canActivate: [AuthGuard] },
];

@NgModule({
  imports: [RouterModule.forRoot(routes, { enableTracing: false })],
  exports: [RouterModule]
})

export class AppRoutingModule { }
