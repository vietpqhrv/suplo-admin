
import { Component, OnInit, ViewChild } from '@angular/core';
import { MatPaginator, MatTableDataSource, MatSort } from '@angular/material';
import { MatDialog, MatDialogConfig } from '@angular/material';
import { SelectionModel } from '@angular/cdk/collections';
import { FirestoreService } from '../../../services/database-service/firestore/firestore.service';
import { isUndefined } from 'util';

// khai báo biến của table
export interface PeriodicElement {
  name: string;
  phone: number;
  company: number;
  email: string;
  node: string;
  value_money: string;
  value_project;
  value_project1;
  value_project2;
  value_project3;
  value_project4;
}

//danh sách người dùng trong table


@Component({
  selector: 'app-user-list',
  templateUrl: './user-list.component.html',
  styleUrls: ['./user-list.component.scss']
})

export class UserListComponent implements OnInit {

  name;
  phone;
  company;
  email;
  node;
  value_money;
  value_project;
  value_project1;
  value_project2;
  value_project3;
  value_project4;
  lastName;
  firstName;
  address1;
  district;
  city;
  country;
  kiot;
  Confirmed;
  userData = [];
  space1;
  space2;
  space3;
  space4;
  displayedColumns: string[] = ['select', 'name', 'phone', 'company', 'email', 'node', 'value_money', 'value_project'];
  dataSource: MatTableDataSource<any>;
  selection = new SelectionModel<PeriodicElement>(true, []);

  @ViewChild(MatSort) sort: MatSort;
  @ViewChild(MatPaginator) paginator: MatPaginator;

  constructor(
    public dialog: MatDialog,
    private ftSerive: FirestoreService,
  ) { }

  ngOnInit() {
    this.loadData();
  }

  loadData() {
    this.ftSerive.getUserList().onSnapshot(userSnapshot => {
      this.userData = [];
      userSnapshot.forEach(doc => {
        let userInfo = doc.data();
        console.log(userInfo);
        if (userInfo.name != '') {
          this.name = userInfo.name;
        } else {
          this.name = '';
        }

        if (userInfo.email != '') {
          this.email = userInfo.email;
        } else {
          this.email = '';
        }

        if (userInfo.company != '') {
          this.company = userInfo.company;
        } else {
          this.company = '';
        }

        if (userInfo.phoneNumber != '') {
          this.phone = userInfo.phone.replace("+84", "0");
        } else {
          this.phone = '';
        }
        if (userInfo.node != '') {
          this.node = userInfo.node;
        } else {
          this.node = '';
        }

        if (userInfo.value_money != '') {
          this.value_money = userInfo.value_money;
          if(this.value_money == 'lớn hơn 1 đồng'){
            this.value_money = 'lớn hơn 1 tỷ đồng'
          }
        } else {
          this.value_money = '';
        }

        if (userInfo.value_project.value_project1 == true) {
          this.value_project1 = 'Tích hợp hệ thống';
        } else {
          this.value_project1 = '';
        }
        if (userInfo.value_project.value_project2 == true) {
          this.value_project2 = 'Xây dựng website';
        } else {
          this.value_project2 = '';
        }
        if (userInfo.value_project.value_project3 == true) {
          this.value_project3 = 'Xây dựng Mobile App';
        } else {
          this.value_project3 = '';
        }
        if (userInfo.value_project.value_project4 == true) {
          this.value_project4 = 'Tư vấn hỗ trợ';
        } else {
          this.value_project4 = '';
        }

        let kiotData1 = {
          // name: `${this.lastName} ${this.firstName}`,
          name: this.name,
          phone: this.phone,
          company: this.company,
          email: this.email,
          node: this.node,
          value_money: this.value_money,
          value_project: {
            value_project1: this.value_project1,
            value_project2: this.value_project2,
            value_project3: this.value_project3,
            value_project4: this.value_project4,
          },
          // address: ` ${this.address1} ${this.space1} ${this.district} ${this.space2} ${this.city} ${this.space3} ${this.country}`,
        };

        this.userData.push(kiotData1);
        console.log('kiotData1',kiotData1);

      });
      this.dataSource = new MatTableDataSource<PeriodicElement>(this.userData);
      this.dataSource.sort = this.sort;
      this.dataSource.paginator = this.paginator;
    })
  }

  isAllSelected() {
    const numSelected = this.selection.selected.length;
    const numRows = this.dataSource.data.length;
    return numSelected === numRows;
  }

  masterToggle() {
    this.isAllSelected() ?
      this.selection.clear() :
      this.dataSource.data.forEach(row => this.selection.select(row));
  }

  // search người dùng trong table
  applyFilter(filterValue: string) {
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }

  // animal: string;
  // name: string;


  // openFormUpdateUser(user) {
  //   this.ftSerive.setValueFormPopup(user);
  //   let dialogconfig = new MatDialogConfig();
  //   // dialogconfig.disableClose = true;
  //   dialogconfig.autoFocus = true;
  //   dialogconfig.width = "60%";
  //   this.dialog.open(FormComponent, dialogconfig);
  //   console.log(user.uid);
  // }

  // openPopupdeleteUser(user) {
  //   let dialogconfig = new MatDialogConfig();
  //   dialogconfig.disableClose = true;
  //   dialogconfig.autoFocus = true;
  //   dialogconfig.width = "20%";
  //   this.dialog.open(DeleteUserComponent, dialogconfig);
  //   console.log(user.uid);
  //   this.ftSerive.getUidOfDeleteUser(user.uid);
  // }
  // if (userInfo.address != '') {
  //   if (userInfo.address.address1 != '' && !isUndefined(userInfo.address.address1)) {
  //     this.address1 = userInfo.address.address1;
  //     this.space1 = '-';
  //   } else {
  //     this.address1 = '';
  //     this.space1 = '';
  //   }

  //   if (userInfo.address.district != '' && !isUndefined(userInfo.address.district)) {
  //     this.district = `  ${userInfo.address.district}`;
  //     this.space2 = '-';
  //   } else {
  //     this.district = '';
  //     this.space2 = '';
  //   }

  //   if (userInfo.address.city != '' && !isUndefined(userInfo.address.city)) {
  //     this.city = ` ${userInfo.address.city}`;
  //     this.space3 = '-';
  //   } else {
  //     this.city = '';
  //     this.space3 = '';
  //   }

  //   if (userInfo.address.country != '' && !isUndefined(userInfo.address.country)) {
  //     this.country = `${userInfo.address.country}`;
  //   } else {
  //     this.country = '';
  //   }

  // } else {
  //   this.country = '';
  //   this.district = '';
  //   this.address1 = '';
  //   this.city = '';
  //   this.space1 = '';
  //   this.space2 = '';
  //   this.space3 = '';
  // }
}
