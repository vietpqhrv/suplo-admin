import { Component, OnInit } from '@angular/core';
import { MatDialogRef } from '@angular/material';
import { MatSnackBar } from '@angular/material';
import { FirestoreService } from 'src/app/services/database-service/firestore/firestore.service';
@Component({
  selector: 'app-delete-user',
  templateUrl: './delete-user.component.html',
  styleUrls: ['./delete-user.component.scss']
})
export class DeleteUserComponent implements OnInit {

  constructor(
    public dialogRef: MatDialogRef<DeleteUserComponent>,
    private snackBar: MatSnackBar,
    private fsService: FirestoreService,
  ) { }

  ngOnInit() {

  }

  closeForm() {
    this.dialogRef.close();
  }

  updateConfirmed() {
    this.fsService.updateConfirmed().then(data => {
      console.log('Xác nhận thành công');
      this.closeForm();
      this.openSnackBar('Xác nhận thành công');
    })
  }

  openSnackBar(txt) {
    this.snackBar.open(txt, '', {
      duration: 2000,
    });
  }

}
