import { Component, OnInit, ViewChild } from '@angular/core';
import { MatPaginator, MatTableDataSource, MatSort } from '@angular/material';
import { MatDialog, MatDialogConfig } from '@angular/material';
import { SelectionModel } from '@angular/cdk/collections';
// import { FormComponent } from './modal/form/form.component';
// import { FirestoreService } from '../../../services/database-service/firestore/firestore.service';
import { DeleteUserComponent } from '../../users/user-list/modal/delete-user/delete-user.component';
import { FirestoreService } from '../../../services/database-service/firestore/firestore.service';
import { isUndefined } from 'util';

// khai báo biến của table
export interface PeriodicElement {
  name: string;
  phone: number;
  brithday: number;
  address: string;
}

//danh sách người dùng trong table


@Component({
  selector: 'app-kiot-list',
  templateUrl: './kiot-list.component.html',
  styleUrls: ['./kiot-list.component.scss']
})
export class KiotListComponent implements OnInit {
  name;
  phone;
  birthday;
  address;
  lastName;
  firstName;
  kiotData = [];
  address1;
  district;
  city;
  country;
  kiot;
  Confirmed;
  space1;
  space2;
  space3;
  space4;
  displayedColumns: string[] = ['select', 'name', 'phone', 'birthday', 'address', 'kiot'];
  dataSource: MatTableDataSource<any>;
  selection = new SelectionModel<PeriodicElement>(true, []);

  @ViewChild(MatSort) sort: MatSort;
  @ViewChild(MatPaginator) paginator: MatPaginator;

  constructor(
    public dialog: MatDialog,
    private ftSerive: FirestoreService,
  ) { }

  ngOnInit() {
    this.loadData();
  }

  loadData() {
    this.ftSerive.getKiotList().onSnapshot(kiotSnapshot => {
      this.kiotData = [];
      kiotSnapshot.forEach(doc => {
        let kiotInfo = doc.data();
        let owner = kiotInfo.owner;
        if (kiotInfo.owner) {
          this.firstName = kiotInfo.owner.firstName;
          this.lastName = kiotInfo.owner.lastName;
          this.birthday = kiotInfo.owner.birthDay;
        } else {
          this.lastName = '';
          this.firstName = '';
          this.birthday = '';
        }

        if (owner) {
          if (owner.address.address1 != '' && !isUndefined(owner.address.address1)) {
            this.address1 = `${owner.address.address1}`;
            this.space1 = '-';
          } else {
            this.address1 = '';
            this.space1 = '';
          }

          if (owner.address.city != '' && !isUndefined(owner.address.city)) {
            this.city = `${owner.address.city}`;
            this.space2 = '-';
          } else {
            this.city = '';
            this.space2 = '';
          }

          if (owner.address.district != '' && !isUndefined(owner.address.district)) {
            this.district = `${owner.address.district}`;
            this.space3 = '-';
          } else {
            this.district = '';
            this.space3 = '';
          }

          if (owner.address.country != '' && !isUndefined(owner.address.country)) {
            this.country = `${owner.address.country}`;
            this.space4 = '-';
          } else {
            this.country = '';
            this.space4 = '';
          }
          
        } else {
          this.address1 = '';
          this.country = '';
          this.district = '';
          this.city = '';
          this.Confirmed = true;
          this.space1 = '';
          this.space2 = '';
          this.space3 = '';
        }
        this.kiot = doc.id;
        this.Confirmed = kiotInfo.isConfirmed;
        this.phone = kiotInfo.phoneNumber.replace("+84", "0");
        let kiotData1 = {
          name: `${this.lastName} ${this.firstName}`,
          phone: this.phone,
          birthday: this.birthday,
          address: ` ${this.address1} ${this.space1} ${this.district} ${this.space2} ${this.city} ${this.space3} ${this.country}`,
          kiot: this.kiot,
          Confirmed: this.Confirmed,
        };

        this.kiotData.push(kiotData1);
        console.log(kiotData1);
      });
      this.dataSource = new MatTableDataSource<PeriodicElement>(this.kiotData);
      this.dataSource.sort = this.sort;
      this.dataSource.paginator = this.paginator;
    })
  }

  isAllSelected() {
    const numSelected = this.selection.selected.length;
    const numRows = this.dataSource.data.length;
    return numSelected === numRows;
  }

  masterToggle() {
    this.isAllSelected() ?
      this.selection.clear() :
      this.dataSource.data.forEach(row => this.selection.select(row));
  }

  // search người dùng trong table
  applyFilter(filterValue: string) {
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }

  // animal: string;
  // name: string;


  // openFormUpdateUser(user) {
  //   this.ftSerive.setValueFormPopup(user);
  //   let dialogconfig = new MatDialogConfig();
  //   // dialogconfig.disableClose = true;
  //   dialogconfig.autoFocus = true;
  //   dialogconfig.width = "60%";
  //   this.dialog.open(FormComponent, dialogconfig);
  //   console.log(user.uid);
  // }

  openPopupdeleteUser(user) {
    let dialogconfig = new MatDialogConfig();
    dialogconfig.disableClose = true;
    dialogconfig.autoFocus = true;
    dialogconfig.width = "40%";
    this.dialog.open(DeleteUserComponent, dialogconfig);
    this.ftSerive.updateConfirmedKiot(user.kiot);
    console.log(user.kiot);
  }

}
