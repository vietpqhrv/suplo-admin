import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, RouterStateSnapshot, Router, CanActivate } from '@angular/router';
import { Observable } from 'rxjs';
import * as firebase from 'firebase/app';
import 'firebase/auth';

@Injectable({
  providedIn: 'root'
})



export class AuthGuard implements CanActivate {

  constructor(
    private router: Router,
  ) { }
  canActivate(next: ActivatedRouteSnapshot, state: RouterStateSnapshot): boolean | Observable<boolean> | Promise<boolean> {
    return new Promise((resolve, reject) => {
      firebase.auth().onAuthStateChanged((user: firebase.User) => {
        if (user) {
          console.log(user);
          resolve(true);
        }
        else {
          this.router.navigate(['/login']);
          // alert('bạn chưa đăng nhập');
          resolve(false);
        }
      })
    })
  }
}