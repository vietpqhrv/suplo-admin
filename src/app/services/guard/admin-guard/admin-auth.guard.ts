import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, RouterStateSnapshot, Router, CanActivate } from '@angular/router';
import { Observable } from 'rxjs';
import { AuthService } from 'src/app/services/auth-service/auth.service';
import { FirestoreService } from '../../../services/database-service/firestore/firestore.service';
import * as firebase from 'firebase/app';
import 'firebase/auth';
@Injectable({
  providedIn: 'root'
})

export class AdminAuthGuard implements CanActivate {
  user;
  db = firebase.firestore();
  constructor(
    public router: Router,
    private authService: AuthService,
    private ftService: FirestoreService,
  ) { }
  canActivate(next: ActivatedRouteSnapshot, state: RouterStateSnapshot): boolean | Observable<boolean> | Promise<boolean> {
    return new Promise((resolve, reject) => {
      this.authService.checkoutAdmin().onAuthStateChanged(user => {
        this.ftService.getDataCheckadmin(user).then(user1 => {
          let auth = user1.data();
          if (auth.role == 'user') {
            this.router.navigate(['/']);
            resolve(false);
          } else if (auth.role == 'admin') {
            resolve(true);
          }
        }).catch(function (error) {
          console.log("Error getting document:", error);
        });
      })
    })
  }
}
