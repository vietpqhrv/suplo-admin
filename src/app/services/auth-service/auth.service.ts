import { Injectable } from '@angular/core';
import { AngularFireAuth } from 'angularfire2/auth';
import * as firebase from 'firebase/app';
import { Observable } from 'rxjs';
import 'firebase/auth';


@Injectable({
  providedIn: 'root'
})
export class AuthService {
  nameUse: Observable<firebase.User>;
  auth: any;

  constructor(
    private fireAuth: AngularFireAuth,
  ) {
    this.nameUse = fireAuth.authState;
  }

  async register(email, password) {
    return await firebase.auth().createUserWithEmailAndPassword(email, password)
  }

  async delete() {
    var user = firebase.auth().currentUser;

    return await user.delete().then(function () {
      // User deleted.
    }).catch(function (error) {
      // An error happened.
    });
  }

  async login(email: string, password: string) {
    return await this.fireAuth.auth.signInWithEmailAndPassword(email, password)
  }

  async logout() {
    return await this.fireAuth.auth.signOut();
  }

  checkoutAdmin() {
    return firebase.auth();
  }
}
