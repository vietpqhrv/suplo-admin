import { Injectable } from '@angular/core';
import * as firebase from 'firebase/app';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import 'firebase/firestore';

@Injectable({
  providedIn: 'root'
})
export class FirestoreService {

  kiot;
  form1;
  form = new FormGroup({
    email: new FormControl('', [Validators.required, Validators.email]),
    password: new FormControl('', [Validators.required, Validators.minLength(6)]),
  });

  public usersListRef;
  public kiotListRef;
  constructor() {
    this.usersListRef = firebase.firestore().collection(`register`);
    this.kiotListRef = firebase.firestore().collection(`kiots`);
  }


  getKiotList() {
    return this.kiotListRef;
  }

  getUserList() {
    return this.usersListRef;
  }

  async getUserProfile() {
    return await this.usersListRef.doc();
  }

  async getDataCheckadmin(data) {
    return await this.usersListRef.doc(data.uid).get();
  }

  async getUserpopup(user) {
    return await this.usersListRef.doc(user).get();
  }

  async updateConfirmed() {
    return this.kiotListRef.doc(this.kiot).update({
      isConfirmed: true
    });
  }

  // setValueFormPopup(user) {
  //   this.form1 = user;
  //   this.uid = user.uid;
  //   this.form.setValue({
  //     email: user.email,
  //     password: user.password,
  //   })
  // }

  // async updateUser(form) {
  //   return await this.usersListRef.doc(this.uid).update({
  //     email: form.email,
  //     password: form.password,
  //   });
  // }

  updateConfirmedKiot(kiot) {
    this.kiot = kiot;
  }

  // async deleteUser() {
  //   console.log(this.uid);
  //   return await this.usersListRef.doc(this.uid).delete();
  // }

}
