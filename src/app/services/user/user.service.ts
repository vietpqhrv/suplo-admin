import { Injectable } from '@angular/core';
import { AngularFireAuth } from 'angularfire2/auth';
import * as firebase from 'firebase/app';
import { Observable } from 'rxjs';
import 'firebase/auth';
import 'firebase/firestore';

@Injectable({
  providedIn: 'root'
})
export class UserService {

  fs;
  userRef;

  constructor(private firebaseAuth: AngularFireAuth, ) {
    this.fs = firebase.firestore();
    this.userRef = this.fs.collection(`/users`)
  }

  getListUser() {
    return this.userRef;
  }

  async getUserDetail(uid) {
    return await this.userRef.doc(uid).get()
  }
}
