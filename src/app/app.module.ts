import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { ClickOutsideModule } from 'ng-click-outside';

import { LoginComponent } from './account/login/login.component';
import { AuthComponent } from './account/auth/auth.component';
import { AuthService } from './services/auth-service/auth.service';
import { FormsModule } from '@angular/forms';
import { AngularFireModule } from 'angularfire2';
import { environment } from '../environments/environment';
import { AngularFireAuthModule } from 'angularfire2/auth';
import { AuthGuard } from './services/guard/user-guard/auth.guard';
import { RegisterComponent } from './account/register/register.component';
import { ReactiveFormsModule } from '@angular/forms';
import { AngularFirestoreModule } from '@angular/fire/firestore';
import {
  MatDialogModule, MatTooltipModule, MatToolbarModule, MatSidenavModule, MatFormFieldModule, MatExpansionModule, MatPaginatorModule, MatTableModule, MatCheckboxModule, MatIconModule, MatDatepickerModule, MatNativeDateModule, MatInputModule, MatSnackBarModule, MatSortModule, MatMenuModule, MatButtonModule,
} from '@angular/material';

import { AngularFireDatabaseModule } from 'angularfire2/database';
// import { FormComponent } from './pages/users/user-list/modal/form/form.component';
import { DeleteUserComponent } from './pages/users/user-list/modal/delete-user/delete-user.component';
import { UserDetailComponent } from './pages/users/user-detail/user-detail.component';
import { UserListComponent } from './pages/users/user-list/user-list.component';
import { KiotListComponent } from './pages/kiots/kiot-list/kiot-list.component';
@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    AuthComponent,
    RegisterComponent,
    // FormComponent,
    DeleteUserComponent,
    UserDetailComponent,
    UserListComponent,
    KiotListComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    MatSidenavModule,
    MatToolbarModule,
    BrowserAnimationsModule,
    MatIconModule,
    //
    MatButtonModule,
    MatCheckboxModule,
    MatDatepickerModule,
    MatDialogModule,
    MatSnackBarModule,
    MatSortModule,
    MatTableModule,
    //
    ClickOutsideModule,
    MatDatepickerModule,
    MatFormFieldModule,
    MatCheckboxModule,
    MatNativeDateModule,
    MatInputModule,
    MatTooltipModule, MatMenuModule,
    MatDialogModule,
    MatTableModule,
    MatSnackBarModule,
    MatPaginatorModule,
    MatExpansionModule,
    FormsModule,
    AppRoutingModule,
    AngularFireModule.initializeApp(environment.firebase),
    AngularFireAuthModule,
    ReactiveFormsModule,
    AngularFirestoreModule,
    AngularFireDatabaseModule,
  ],

  entryComponents: [
    DeleteUserComponent
  ],
  providers: [
    AuthService,
    AuthGuard,
    MatDatepickerModule,
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
